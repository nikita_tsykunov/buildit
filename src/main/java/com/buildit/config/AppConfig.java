package com.buildit.config;

import com.buildit.rental.application.dto.FullPurchaseOrderDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableIntegration
class Flows {

    @Bean
    IntegrationFlow scatterComponent() {
        return IntegrationFlows.from("find-plants")
                .publishSubscribeChannel(conf ->
                                conf.applySequence(true)
                                    .subscribe(f -> f.channel("rentit2-req"))
                                    .subscribe(f -> f.channel("rentit-req"))
                )
                .get();
    }

    @Bean
    IntegrationFlow rentItFlow() {
        return IntegrationFlows.from("rentit-req")
                .enrichHeaders(h -> h.header("supplier", "RentIt"))
                .handle("rentalServiceImpl", "findAvailablePlants")
                .channel("gather-channel")
                .get();
    }

    @Bean
    IntegrationFlow rentIt2Flow() {
        return IntegrationFlows.from("rentit2-req")
                .enrichHeaders(h -> h.header("supplier", "Team8"))
                .handle("rentalServiceImpl", "findAvailablePlants")
                .channel("gather-channel")
                .get();
    }

    @Bean
    IntegrationFlow createPOFlow() {
        return IntegrationFlows.from("create-po")
                .enrichHeaders(h -> h.headerExpression("supplier", "payload.supplierId"))
                .handle("rentalServiceImpl", "createPO")
                .channel("rep-channel")
                .get();
    }

    @Bean
    IntegrationFlow modifyPOFlow() {
        return IntegrationFlows.from("modify-po")
                .enrichHeaders(h -> h.headerExpression("supplier", "payload.supplierId"))
                .handle("rentalServiceImpl", "modifyPO")
                .channel("rep-channel")
                .get();
    }

    @Bean
    IntegrationFlow extendPOFlow() {
        return IntegrationFlows.from("extend-po")
                .handle("rentalServiceImpl", "extendPO")
                .channel("rep-channel")
                .get();
    }

    @Bean
    IntegrationFlow cancelPOFlow() {
        return IntegrationFlows.from("cancel-po")
                .handle("rentalServiceImpl", "cancelPO")
                .channel("rep-channel")
                .get();
    }

    @Bean
    IntegrationFlow sendAdviceFlow() {
        return IntegrationFlows.from("send-advice")
                .enrichHeaders(h -> h.headerExpression("supplier", "payload.supplierId"))
                .handle("rentalServiceImpl", "sendRemittanceAdvice")
                .channel("rep-channel")
                .get();
    }

    @Bean
    IntegrationFlow findPOsFlow() {
        return IntegrationFlows.from("find-pos")
                .publishSubscribeChannel(conf ->
                        conf.applySequence(true)
                                .subscribe(f -> f.channel("find-pos-rentit"))
                                .subscribe(f -> f.channel("find-pos-team8"))
                )
                .get();
    }

    @Bean
    IntegrationFlow findPOsRentIt() {
        return IntegrationFlows.from("find-pos-rentit")
                .enrichHeaders(h -> h.header("supplier", "RentIt"))
                .handle("rentalServiceImpl", "findPurchaseOrders")
                .channel("gather-pos")
                .get();
    }

    @Bean
    IntegrationFlow findPOsTeam8() {
        return IntegrationFlows.from("find-pos-team8")
                .enrichHeaders(h -> h.header("supplier", "Team8"))
                .handle("rentalServiceImpl", "findPurchaseOrders")
                .channel("gather-pos")
                .get();
    }

    @Bean
    IntegrationFlow gatehrPOs() {
        return IntegrationFlows.from("gather-pos")
                .resequence()
                .aggregate(a -> a.outputProcessor(mg -> {
                    List<FullPurchaseOrderDTO> orders = new ArrayList<>();
                    for(Message<?> message: mg.getMessages()){
                        orders.addAll((List<FullPurchaseOrderDTO>) message.getPayload());
                    }

                    return new GenericMessage<List<FullPurchaseOrderDTO>>(orders);
                }))
                .channel("rep-channel")
                .get();
    }

    @Bean
    IntegrationFlow gatherComponent() {
        return IntegrationFlows.from("gather-channel")
                .resequence()
                .aggregate(a -> a.outputProcessor(mg -> {
                    List<PlantInventoryEntryDTO> plants = new ArrayList<>();
                    for(Message<?> message: mg.getMessages()){
                        plants.addAll((List<PlantInventoryEntryDTO>) message.getPayload());
                    }

                    return new GenericMessage<List<PlantInventoryEntryDTO>>(plants);
                }))
                .channel("rep-channel")
                .get();
    }
}

@Configuration
class ObjectMapperCustomizer {
    @Autowired
    @Qualifier("_halObjectMapper")
    private ObjectMapper springHateoasObjectMapper;

    @Bean(name = "objectMapper")
    ObjectMapper objectMapper() {
        return springHateoasObjectMapper
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
                .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .registerModules(new JavaTimeModule());
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate _restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        messageConverters.add(new MappingJackson2HttpMessageConverter(springHateoasObjectMapper));
        _restTemplate.setMessageConverters(messageConverters);
        return _restTemplate;
    }

}


@Configuration
@EnableWebMvc
class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://ec2-13-53-184-20.eu-north-1.compute.amazonaws.com")
                .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
    }

}

@Configuration
class EndpointConfig {

    @Autowired
    @Qualifier("_halObjectMapper")
    ObjectMapper objectMapper;

    @Bean
    public JsonNode config() throws IOException {

        InputStream is = new FileInputStream(ResourceUtils.getFile("classpath:config.json"));

        return objectMapper.readValue(is, new TypeReference<JsonNode>() {});
    }
}
