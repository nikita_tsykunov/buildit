package com.buildit.procurement.rest;

import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.application.service.ProcurementService;
import com.buildit.rental.application.dto.FullPurchaseOrderDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.service.IRentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/procurements")
@CrossOrigin
public class ProcurementRestController {

    @Autowired
    ConfigurableApplicationContext ctx;

    private IRentalService rentalService;

    @PostConstruct
    public void init(){
        rentalService = ctx.getBean(IRentalService.class);
    }

    @Autowired
    ProcurementService procurementService;

    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name", required = false) Optional<String> plantName,
            @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
            @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate
    ) {
        return rentalService.findAvailablePlants(plantName.get(), startDate.get(), endDate.get());
    }

    @GetMapping("/orders")
    public List<FullPurchaseOrderDTO> findPurchaseOrders() {
        return rentalService.findPurchaseOrders();
    }

    @GetMapping("/phrs")
    public List<PlantHireRequestDTO> findPHRs() {
        return procurementService.findAllPHRs();
    }

    @GetMapping("/phrs/invoice")
    public Collection<PlantHireRequestDTO> findInvoiced() {
        return procurementService.findInvoiced();
    }

    @GetMapping(value = "/phrs/{id}")
    public PlantHireRequestDTO fetchPHR(@PathVariable("id") Long id) {
        return procurementService.getPHR(id);
    }

    @PostMapping("/phrs")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<?> createPHR(@RequestBody PlantHireRequestDTO phrDTO){
        return new ResponseEntity<>(procurementService.createPHR(phrDTO), HttpStatus.CREATED);
    }

    @PostMapping("/phrs/{id}/accept")
    public ResponseEntity<PlantHireRequestDTO> acceptPHR(@PathVariable Long id){
        return new ResponseEntity<>(procurementService.acceptPHR(id), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/cancel")
    public ResponseEntity<PlantHireRequestDTO> cancelPHR(@PathVariable("id") Long id) {
        return new ResponseEntity<>(procurementService.cancelPHR(id), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/reject")
    public ResponseEntity<PlantHireRequestDTO> rejectPHR(@PathVariable("id") Long id, @RequestBody(required = false) String comment){
        return new ResponseEntity<>(procurementService.rejectPHR(id, comment), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/extend")
    public ResponseEntity<PlantHireRequestDTO> extendPHR(@PathVariable("id") Long id, @RequestBody  LocalDate date){
        return new ResponseEntity<>(procurementService.extendPHR(id, date), HttpStatus.OK);
    }

    @PatchMapping("/phrs/{id}")
    public ResponseEntity<PlantHireRequestDTO> modifyPHR(@PathVariable("id") Long id, @RequestBody PlantHireRequestDTO phr){
       return new ResponseEntity<>(procurementService.modifyPHR(id, phr), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/po/accept")
    public ResponseEntity<PlantHireRequestDTO> notifyAcceptPO(@PathVariable Long id){
        return new ResponseEntity<>(procurementService.notifyAcceptPO(id), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/po/reject")
    public ResponseEntity<PlantHireRequestDTO> notifyRejectPO(@PathVariable("id") Long id){
        return new ResponseEntity<>(procurementService.notifyRejectPO(id), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/invoice")
    public ResponseEntity<PlantHireRequestDTO> receiveInvoice(@PathVariable("id") Long id) {
        return new ResponseEntity<>(procurementService.receiveInvoice(id), HttpStatus.CREATED);
    }

    @PostMapping("/phrs/{id}/invoice/accept")
    public ResponseEntity<PlantHireRequestDTO> acceptInvoice(@PathVariable("id") Long id) {
        return new ResponseEntity<>(procurementService.acceptInvoice(id), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/invoice/reject")
    public ResponseEntity<PlantHireRequestDTO> rejectInvoice(@PathVariable("id") Long id) {
        return new ResponseEntity<>(procurementService.rejectInvoice(id), HttpStatus.OK);
    }

    @PostMapping("/phrs/{id}/invoice/remind")
    public ResponseEntity<PlantHireRequestDTO> receiveReminder(@PathVariable("id") Long id) {
        return new ResponseEntity<>(procurementService.receiveReminder(id), HttpStatus.OK);
    }

    //Only for cucumber tests
    @DeleteMapping("/phrs")
    public void deleteAllPHRs() {
        procurementService.deleteAllPHRs();
    }
}
