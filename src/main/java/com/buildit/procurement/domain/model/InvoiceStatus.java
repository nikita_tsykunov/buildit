package com.buildit.procurement.domain.model;

public enum InvoiceStatus {
    UNPAID, APPROVED, REJECTED, LATE_PAYMENT, PAID
}
