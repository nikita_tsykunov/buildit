package com.buildit.procurement.domain.model;

import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.rental.domain.model.PlantInventoryEntry;
import com.buildit.rental.domain.model.PurchaseOrder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PlantHireRequest {
    @Id
    @GeneratedValue
    Long id;

    Long siteId;
    String supplierId;

    @Embedded
    PlantInventoryEntry plant;

    @Embedded
    BusinessPeriod period;

    @Column(precision=8,scale=2)
    BigDecimal cost;

    @Enumerated(EnumType.STRING)
    PHRStatus status;

    String comment;

    @Embedded
    PurchaseOrder purchaseOrder;

    String siteEngineerName;

    @OneToOne(cascade = CascadeType.ALL)
    Invoice invoice;

    public static PlantHireRequest of(Long siteId, String supplierId, PlantInventoryEntry plant, BusinessPeriod period,
                                      BigDecimal cost, String siteEngineerName) {
        PlantHireRequest phr = new PlantHireRequest();
        phr.siteId = siteId;
        phr.supplierId = supplierId;
        phr.plant = plant;
        phr.period = period;
        phr.status = PHRStatus.PENDING;
        phr.cost = cost;
        phr.siteEngineerName = siteEngineerName;
        return phr;
    }

    public void setStatus(PHRStatus newStatus) {
        status = newStatus;
    }

    public void setPurchaseOrder(PurchaseOrder po){
        purchaseOrder = po;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public void setPeriod(BusinessPeriod period) {
        this.period = period;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public void setSiteEngineerName(String siteEngineerName){
        this.siteEngineerName = siteEngineerName;
    }
}
