package com.buildit.procurement.domain.model;

public enum PHRStatus {
    PENDING, ACCEPTED_BY_ENGINEER, REJECTED_BY_ENGINEER, ACCEPTED_BY_SUPPLIER, REJECTED_BY_SUPPLIER, CANCELED
}
