package com.buildit.procurement.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.rest.ResourceSupport;
import com.buildit.procurement.domain.model.PHRStatus;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlantHireRequestDTO extends ResourceSupport {
    Long _id;
    Long siteId;
    String supplierId;
    PlantInventoryEntryDTO plant;
    BusinessPeriodDTO period;
    BigDecimal cost;
    PHRStatus status;
    String comment;
    PurchaseOrderDTO purchaseOrder;
    String siteEngineerName;
    InvoiceDTO invoice;
}
