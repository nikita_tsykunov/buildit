package com.buildit.procurement.application.dto;

import com.buildit.common.rest.ResourceSupport;
import com.buildit.procurement.domain.model.InvoiceStatus;
import lombok.Data;

import java.time.LocalDate;

@Data
public class InvoiceDTO extends ResourceSupport {
    Long _id;
    InvoiceStatus status;
    LocalDate invoiceDate;
}
