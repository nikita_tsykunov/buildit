package com.buildit.procurement.application.service;

import com.buildit.procurement.application.dto.InvoiceDTO;
import com.buildit.procurement.domain.model.Invoice;
import com.buildit.procurement.rest.ProcurementRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceDTO> {

    public InvoiceAssembler() { super(ProcurementRestController.class, InvoiceDTO.class); }

    @Override
    public InvoiceDTO toResource(Invoice invoice) {
        InvoiceDTO dto = new InvoiceDTO();

        dto.set_id(invoice.getId());
        dto.setInvoiceDate(invoice.getInvoiceDate());
        dto.setStatus(invoice.getStatus());

        return dto;
    }
}
