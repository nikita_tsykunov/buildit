package com.buildit.procurement.application.service;

import com.buildit.common.application.service.BusinessPeriodValidator;
import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.domain.model.Invoice;
import com.buildit.procurement.domain.model.InvoiceStatus;
import com.buildit.procurement.domain.model.PHRStatus;
import com.buildit.procurement.domain.model.PlantHireRequest;
import com.buildit.procurement.domain.repository.PlantHireRequestRepository;
import com.buildit.rental.application.dto.FullPurchaseOrderDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.service.IRentalService;
import com.buildit.rental.domain.model.POStatus;
import com.buildit.rental.domain.model.PlantInventoryEntry;
import com.buildit.rental.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.DataBinder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class ProcurementService {
    @Autowired
    PlantHireRequestRepository phrRepository;

    @Autowired
    PlantHireRequestAssembler phrAssembler;

    @Autowired
    ConfigurableApplicationContext ctx;

    private IRentalService rentalService;

    @PostConstruct
    public void init(){
        rentalService = ctx.getBean(IRentalService.class);
    }

    @Autowired
    RestTemplate restTemplate;

    public List<PlantHireRequestDTO> findAllPHRs() {
        return phrAssembler.toResources(phrRepository.findAll());
    }

    public Collection<PlantHireRequestDTO> findInvoiced() {
        return phrAssembler.toResources(phrRepository.findInvoiced());
    }

    public PlantHireRequestDTO getPHR(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");

        return phrAssembler.toResource(phr);
    }

    public PlantHireRequestDTO createPHR(PlantHireRequestDTO phrDTO) {
        BusinessPeriod period = BusinessPeriod.of(
                phrDTO.getPeriod().getStartDate(),
                phrDTO.getPeriod().getEndDate());

        validateBusinessPeriod(phrDTO);

        PlantInventoryEntry plant = PlantInventoryEntry.of(phrDTO.getPlant().getHref(),
                phrDTO.getPlant().getName(), phrDTO.getPlant().get_id(), phrDTO.getPlant().getPrice());

        BigDecimal cost = phrDTO.getCost();

        PlantHireRequest phr = PlantHireRequest.of(phrDTO.getSiteId(), phrDTO.getSupplierId(), plant, period, cost,
                phrDTO.getSiteEngineerName());
        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    public PlantHireRequestDTO rejectPHR(Long id, String comment) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");

        if (phr.getStatus() != PHRStatus.PENDING)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PHR cannot be rejected because it is not Pending");

        phr.setStatus(PHRStatus.REJECTED_BY_ENGINEER);
        if (comment != null){
            phr.setComment(comment);
        }
        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }
    public PlantHireRequestDTO cancelPHR(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);
        POStatus status = POStatus.CLOSED;
        if (phr == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        if (phr.getStatus() != PHRStatus.PENDING){
            status = rentalService.cancelPO(phr.getPurchaseOrder().getPo_href());
        }

        if (status == POStatus.CLOSED){
            phr.setStatus(PHRStatus.CANCELED);
            phrRepository.save(phr);
        }

        return phrAssembler.toResource(phr);
    }
    public PlantHireRequestDTO extendPHR(Long id, LocalDate date) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);
        if (phr == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");

        rentalService.extendPO(phr.getPurchaseOrder().getPo_href(), date);
        phr.setStatus(PHRStatus.ACCEPTED_BY_ENGINEER);
        BusinessPeriod period = BusinessPeriod.of(
                phr.getPeriod().getStartDate(),date);
        phr.setPeriod(period);
        phrRepository.save(phr);
        return phrAssembler.toResource(phr);
    }

    public PlantHireRequestDTO acceptPHR(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (phr.getStatus() != PHRStatus.PENDING) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PHR cannot be accepted because it is not Pending");
        }

        if (phr.getPurchaseOrder() == null) {
            PurchaseOrder createdPO = rentalService.createPO(phrAssembler.toResource(phr));
            phr.setPurchaseOrder(createdPO);
        }
        else {
            PurchaseOrder modifiedPO = rentalService.modifyPO(phrAssembler.toResource(phr));
            phr.setPurchaseOrder(modifiedPO);
        }


        phr.setStatus(PHRStatus.ACCEPTED_BY_ENGINEER);
        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    public PlantHireRequestDTO modifyPHR(Long id, PlantHireRequestDTO newPHR) {
        PlantHireRequest oldPHR = phrRepository.findById(id).orElse(null);

        if (oldPHR == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");

        if (oldPHR.getStatus() == PHRStatus.REJECTED_BY_ENGINEER)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PHR cannot be modified because it is Rejected");

        if (newPHR.getSiteId() != null)
            oldPHR.setSiteId(newPHR.getSiteId());

        validateBusinessPeriod(newPHR);

        if (newPHR.getPeriod() != null) {
            List<PlantInventoryEntryDTO> availablePlants = rentalService.findAvailablePlants(oldPHR.getPlant().getPlant_name(), newPHR.getPeriod().getStartDate(),
                    newPHR.getPeriod().getEndDate());
            if(!availablePlants.stream()
                    .map(PlantInventoryEntryDTO::get_id)
                    .collect(Collectors.toList())
                    .contains(oldPHR.getPlant().getPlant_id()))
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Plant is not available");

            oldPHR.setPeriod(BusinessPeriod.of(newPHR.getPeriod().getStartDate(), newPHR.getPeriod().getEndDate()));
            oldPHR.setCost(oldPHR.getPlant().getPlant_price().multiply(new BigDecimal(DAYS.between(newPHR.getPeriod().getStartDate(),
                    newPHR.getPeriod().getEndDate()) + 1))); //re-calculate cost
        }

        if (newPHR.getComment() != null)
            oldPHR.setComment(newPHR.getComment());

        oldPHR.setStatus(PHRStatus.PENDING);
        phrRepository.save(oldPHR);
        return phrAssembler.toResource(oldPHR);
    }

    public PlantHireRequestDTO notifyAcceptPO(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (phr.getStatus() != PHRStatus.ACCEPTED_BY_ENGINEER) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PO of this PHR cannot be accepted because it is not accepted by works engineer yet");
        }

        phr.setStatus(PHRStatus.ACCEPTED_BY_SUPPLIER);
        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    public PlantHireRequestDTO notifyRejectPO(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (phr.getStatus() != PHRStatus.ACCEPTED_BY_ENGINEER) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PO of this PHR cannot be rejected because it is not accepted by works engineer yet");
        }

        phr.setStatus(PHRStatus.REJECTED_BY_SUPPLIER);
        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    public PlantHireRequestDTO receiveInvoice(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }
        if (phr.getStatus() != PHRStatus.ACCEPTED_BY_SUPPLIER) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "PHR was not accepted");
        }

        FullPurchaseOrderDTO po = restTemplate.getForObject(phr.getPurchaseOrder().getPo_href(), FullPurchaseOrderDTO.class);

        if (po == null || po.getInvoice().getStatus() != InvoiceStatus.UNPAID) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invoice for PO is not UNPAID");
        }

        Invoice invoice = Invoice.of();
        phr.setInvoice(invoice);

        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    @Transactional(propagation= Propagation.REQUIRED, noRollbackFor=Exception.class)
    public PlantHireRequestDTO acceptInvoice(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (phr.getInvoice() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no invoice for this PHR");
        } else if (phr.getInvoice().getStatus() == InvoiceStatus.REJECTED){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invoice can't be approved");
        } else {
            phr.getInvoice().setStatus(InvoiceStatus.APPROVED);
            rentalService.sendRemittanceAdvice(phrAssembler.toResource(phr));
        }

        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    @Transactional(propagation= Propagation.REQUIRED, noRollbackFor=Exception.class)
    public PlantHireRequestDTO rejectInvoice(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (phr.getInvoice() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no invoice for this PHR");
        } else if (phr.getInvoice().getStatus() == InvoiceStatus.APPROVED){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invoice can't be rejected");
        } else {
            phr.getInvoice().setStatus(InvoiceStatus.REJECTED);
        }

        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    @Transactional(propagation= Propagation.REQUIRED, noRollbackFor=Exception.class)
    public PlantHireRequestDTO receiveReminder(Long id) {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (phr.getInvoice() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no invoice for this PHR");
        } else if (phr.getInvoice().getStatus() != InvoiceStatus.UNPAID){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invoice can't be reminded");
        } else {
            phr.getInvoice().setStatus(InvoiceStatus.LATE_PAYMENT);
        }

        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    // Only for Cucumber testing
    public void deleteAllPHRs() {
        phrRepository.deleteAll();
    }

    private void validateBusinessPeriod(PlantHireRequestDTO phr){
        BusinessPeriod period = BusinessPeriod.of(
                phr.getPeriod().getStartDate(),
                phr.getPeriod().getEndDate());
        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid PHR Period");
    }
}
