package com.buildit.procurement.application.service;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.rest.ExtendedLink;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.domain.model.PlantHireRequest;
import com.buildit.procurement.rest.ProcurementRestController;
import com.buildit.rental.application.service.PlantInventoryEntryAssembler;
import com.buildit.rental.application.service.PurchaseOrderAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class PlantHireRequestAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {

    @Autowired
    PlantInventoryEntryAssembler plantAssembler;

    @Autowired
    PurchaseOrderAssembler poAssembler;

    @Autowired
    InvoiceAssembler invoiceAssembler;

    public PlantHireRequestAssembler() {
        super(ProcurementRestController.class, PlantHireRequestDTO.class);
    }

    @Override
    public PlantHireRequestDTO toResource(PlantHireRequest phr) {
        PlantHireRequestDTO dto = new PlantHireRequestDTO();
        dto.set_id(phr.getId());
        dto.setSiteId(phr.getSiteId());
        dto.setSupplierId(phr.getSupplierId());
        dto.setPlant(plantAssembler.toResource(phr.getPlant()));
        dto.setCost(phr.getCost());
        dto.setStatus(phr.getStatus());
        dto.setComment(phr.getComment());
        dto.setPeriod(BusinessPeriodDTO.of(phr.getPeriod().getStartDate(), phr.getPeriod().getEndDate()));
        dto.setSiteEngineerName(phr.getSiteEngineerName());
        if (phr.getPurchaseOrder() != null) {
            dto.setPurchaseOrder(poAssembler.toResource(phr.getPurchaseOrder()));
        }

        if (phr.getInvoice() != null) {
            dto.setInvoice(invoiceAssembler.toResource(phr.getInvoice()));
        }

        dto.add(linkTo(methodOn(ProcurementRestController.class).fetchPHR(dto.get_id())).withSelfRel());

        try {
            switch (phr.getStatus()){
                case PENDING:
                    dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).acceptPHR(dto.get_id())).toString(),
                            "accept", HttpMethod.POST));
                    dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).rejectPHR(dto.get_id(), null)).toString(),
                            "reject", HttpMethod.POST));
                    dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).cancelPHR(dto.get_id())).toString(),
                            "cancel", HttpMethod.POST));
                    dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).modifyPHR(dto.get_id(), null)).toString(),
                            "modify", HttpMethod.PATCH));
                    break;
                case ACCEPTED_BY_ENGINEER:
                    dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).notifyAcceptPO(dto.get_id())).toString(),
                            "notify accept", HttpMethod.POST));
                    dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).notifyRejectPO(dto.get_id())).toString(),
                            "notify reject", HttpMethod.POST));
                    dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).cancelPHR(dto.get_id())).toString(),
                            "cancel", HttpMethod.POST));
                    break;
                case ACCEPTED_BY_SUPPLIER:
                    if (phr.getInvoice() == null) {
                        dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).receiveInvoice(dto.get_id())).toString(),
                                "submit invoice", HttpMethod.POST));
                        dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).cancelPHR(dto.get_id())).toString(),
                                "cancel", HttpMethod.POST));
                        dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).extendPHR(dto.get_id(), LocalDate.now())).toString(),
                                "extend", HttpMethod.POST));
                    }
                    else {
                        switch (phr.getInvoice().getStatus()) {
                            case UNPAID:
                                dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).receiveReminder(dto.get_id())).toString(),
                                        "remind", HttpMethod.POST));
                            case LATE_PAYMENT:
                                dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).rejectInvoice(dto.get_id())).toString(),
                                        "reject", HttpMethod.POST));
                                dto.add(new ExtendedLink(linkTo(methodOn(ProcurementRestController.class).acceptInvoice(dto.get_id())).toString(),
                                        "accept", HttpMethod.POST));
                                break;
                        }
                    }
                    break;
            }
        } catch (Exception e) {}

        return dto;
    }
}
