package com.buildit.common.rest.security;

import com.buildit.common.domain.model.User;
import com.buildit.common.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DataInitializer implements CommandLineRunner {
    @Autowired
    UserRepository users;

    @Override
    public void run(String... args) {
        this.users.save(User.builder()
            .username("admin")
            .password("{noop}admin")
            .fullName("Elon Musk")
            .roles(Arrays.asList("ROLE_ADMIN"))
            .build()
        );
        this.users.save(User.builder()
                .username("we")
                .password("{noop}123")
                .fullName("Snoop Dogg")
                .roles(Arrays.asList("ROLE_WORKS_ENGINEER"))
                .build()
        );
        this.users.save(User.builder()
                .username("se")
                .password("{noop}123")
                .fullName("Jon Snow")
                .roles(Arrays.asList("ROLE_SITE_ENGINEER"))
                .build()
        );
    }
}