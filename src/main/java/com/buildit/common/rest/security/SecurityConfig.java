package com.buildit.common.rest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList("*"));
        configuration.setAllowedMethods(Arrays.asList("HEAD",
                "GET", "POST", "PUT", "DELETE", "PATCH"));
        configuration.setAllowCredentials(true); //!!!
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers().frameOptions().disable()
                .and()
                .httpBasic().disable()
                .csrf().disable()
                .cors()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                    //ONLY FOR CUCUMBER
                    .antMatchers(HttpMethod.DELETE,"/api/procurements/phrs*")
                        .permitAll()
                    //FOR H2 console and logging in
                    .antMatchers("/*", "/h2/**", "/auth/**")
                        .permitAll()

                    //====================== For RentIt =======================
                    //Fetch PHR
                    .antMatchers(HttpMethod.GET, "/api/procurements/phrs/*")
                        .permitAll()
                    //Notify PO accept/reject
                    .antMatchers("/api/procurements/phrs/*/po/**")
                        .permitAll()
                    //Submit invoice
                    .antMatchers(HttpMethod.POST,"/api/procurements/phrs/*/invoice*")
                        .permitAll()
                    //Send invoice reminder
                    .antMatchers(HttpMethod.POST,"/api/procurements/phrs/*/invoice/remind*")
                        .permitAll()

                    //================ For Site Engineers ====================
                    //Create PHRs
                    .antMatchers(HttpMethod.POST, "/api/procurements/phrs*")
                        .hasAnyRole("SITE_ENGINEER", "ADMIN")
                    //Cancel PHRs
                    .antMatchers(HttpMethod.POST, "/api/procurements/phrs/*/cancel*")
                        .hasAnyRole("SITE_ENGINEER", "ADMIN")
                    //Request extensions
                    .antMatchers(HttpMethod.POST, "/api/procurements/phrs/*/extend*")
                        .hasAnyRole("SITE_ENGINEER", "ADMIN")
                    //Approve/reject invoices
                    .antMatchers(HttpMethod.POST, "/api/procurements/phrs/*/invoice/*")
                        .hasAnyRole("SITE_ENGINEER", "ADMIN")

                    //================ For Works Engineers ===================
                    //Accept/reject PHRs
                    .antMatchers(HttpMethod.POST, "/api/procurements/phrs/*/accept*", "/api/procurements/phrs/*/reject*")
                        .hasAnyRole("WORKS_ENGINEER", "ADMIN")

                    //Other requests - for all employees, but not anonymous users
                    .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }
}