package com.buildit.rental.domain.model;

public enum POStatus {
    PENDING, REJECTED, OPEN, CLOSED, PENDING_EXTENSION, DISPATCHED, RETURNED
}
