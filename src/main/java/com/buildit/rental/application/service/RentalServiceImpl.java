package com.buildit.rental.application.service;

import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.application.service.ProcurementService;
import com.buildit.procurement.rest.ProcurementRestController;
import com.buildit.rental.application.dto.FullPurchaseOrderDTO;
import com.buildit.rental.application.dto.POExtensionDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.buildit.rental.domain.model.POStatus;
import com.buildit.rental.domain.model.PurchaseOrder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class RentalServiceImpl{
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ProcurementService procurementService;

    @Autowired
    JsonNode config;

    @Autowired
    @Qualifier("_halObjectMapper")
    ObjectMapper objectMapper;

    public List<PlantInventoryEntryDTO> findAvailablePlants(Message<?> message) throws IOException {
        MessageHeaders headers = message.getHeaders();
        List<PlantInventoryEntryDTO> plants = new ArrayList<>();
        try {
            switch (headers.get("supplier").toString()) {
                case "RentIt": {
                    ResponseEntity<Map<String, Map<String, List<PlantInventoryEntryDTO>>>> response = restTemplate.exchange(
                            config.get(headers.getOrDefault("supplier", "RentIt").toString()).asText() +
                                    "/sales/plants?name={name}&startDate={start}&endDate={end}",
                            HttpMethod.GET, null,
                            new ParameterizedTypeReference<Map<String, Map<String, List<PlantInventoryEntryDTO>>>>() {},
                            message.getPayload(),
                            headers.get("startDate"),
                            headers.get("endDate"));
                        plants.addAll(response.getBody().get("_embedded").get("plantInventoryEntryDToes"));
                        plants.forEach(p -> p.setHref(p.getLink("self").getHref()));

                        break;
                    }
                case "Team8": {
                   ResponseEntity<Map<String, List<PlantInventoryEntryDTO>>> response = restTemplate.exchange(
                            config.get(headers.getOrDefault("supplier", "RentIt").toString()).asText() +
                                    "/sales/plants?name={name}&startDate={start}&endDate={end}",
                            HttpMethod.GET, null,
                            new ParameterizedTypeReference<Map<String, List<PlantInventoryEntryDTO>>>() {},
                            message.getPayload(),
                            headers.get("startDate"),
                            headers.get("endDate"));
                    plants.addAll(response.getBody().get("content"));
                    plants.forEach(p -> p.setHref(p.get_links().get(0).get("href")));

                    break;
                }
                default: {
                    return new ArrayList<>();
                }
            }
        } catch (Exception ex){
            return new ArrayList<>();
        }

        plants.forEach(p -> p.setSupplier(headers.get("supplier").toString()));
        return plants;
    }

    public PurchaseOrder createPO(Message<PlantHireRequestDTO> message) {
        try {
            PlantHireRequestDTO phr = message.getPayload();
            FullPurchaseOrderDTO partialPO = FullPurchaseOrderDTO.of(phr.getPeriod(), phr.getPlant().get_id());
            partialPO.setSupplierName(message.getHeaders().get("supplier").toString());
            partialPO.setPhrHref(linkTo(methodOn(ProcurementRestController.class).fetchPHR(phr.get_id())).toString());
            partialPO.getPlantHireRequest().add(linkTo(methodOn(ProcurementRestController.class).fetchPHR(phr.get_id())).withSelfRel());
            ResponseEntity<FullPurchaseOrderDTO> result = restTemplate.postForEntity(
                    config.get(message.getHeaders().getOrDefault("supplier", "RentIt").toString()).asText() + "/sales/orders",
                    partialPO, FullPurchaseOrderDTO.class);
            FullPurchaseOrderDTO fpDTO = result.getBody();
          return PurchaseOrder.of(fpDTO.get_links().get("self").get("href"));
        } catch (HttpServerErrorException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PHR cannot be accepted because the plant is not available");
        }
    }

    public POStatus cancelPO(Message<String> message) {
        try {
            ResponseEntity<FullPurchaseOrderDTO> result = restTemplate.getForEntity(message.getPayload(), FullPurchaseOrderDTO.class);
            String url = result.getBody().get_links().get("cancel").get("href");
            ResponseEntity<FullPurchaseOrderDTO> cancelledPO = restTemplate.postForEntity(url, null, FullPurchaseOrderDTO.class);
            return cancelledPO.getBody().getStatus();
        } catch (HttpServerErrorException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PHR cannot be canceled because the plant is not available");
        }
    }

    public void extendPO(Message<String> message) {
        try {
            ResponseEntity<FullPurchaseOrderDTO> result = restTemplate.getForEntity(message.getPayload(), FullPurchaseOrderDTO.class);
            String url = result.getBody().get_links().get("extend").get("href");
            POExtensionDTO poExtensionDTO = new POExtensionDTO();
            poExtensionDTO.setEndDate(message.getHeaders().get("date", LocalDate.class));
            restTemplate.postForEntity(url, poExtensionDTO, FullPurchaseOrderDTO.class);
        } catch (HttpServerErrorException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PHR cannot be extended because the plant is not available");
        }
    }

    public PurchaseOrder modifyPO(Message<PlantHireRequestDTO> message) {
        PlantHireRequestDTO phr = message.getPayload();
        FullPurchaseOrderDTO partialPO = FullPurchaseOrderDTO.of(phr.getPeriod(), phr.getPlant().get_id());
        try {
            FullPurchaseOrderDTO result = restTemplate.patchForObject(
                    phr.getPurchaseOrder().getHref(),
                    partialPO, FullPurchaseOrderDTO.class);
            return PurchaseOrder.of(result.get_links().get("self").get("href"));
        } catch (HttpServerErrorException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PHR cannot be modified because the plant is not available");
        }
    }

    public List<FullPurchaseOrderDTO> findPurchaseOrders(Message<?> message) {
        String supplier = message.getHeaders().getOrDefault("supplier", "RentIt").toString();
        List<FullPurchaseOrderDTO> orders = new ArrayList<>();
        try {
            switch (supplier) {
                case "RentIt": {
                    ResponseEntity<Map<String, Map<String, List<FullPurchaseOrderDTO>>>> response = restTemplate.exchange(
                            config.get(supplier).asText() + "/sales/orders?name=" + supplier,
                            HttpMethod.GET, null,
                            new ParameterizedTypeReference<Map<String, Map<String, List<FullPurchaseOrderDTO>>>>() {}, supplier);

                    orders.addAll(response.getBody().get("_embedded").get("purchaseOrderDToes"));
                    break;
                }
                case "Team8": {
                    ResponseEntity<Map<String, List<FullPurchaseOrderDTO>>> response = restTemplate.exchange(
                            config.get(supplier).asText() + "/sales/orders/pending",
                            HttpMethod.GET, null,
                            new ParameterizedTypeReference<Map<String, List<FullPurchaseOrderDTO>>>() {}, supplier);

                    List<String> phrs = procurementService.findAllPHRs().stream().map(p -> p.getPurchaseOrder().getHref()).collect(Collectors.toList());
                    orders.addAll(response.getBody().get("content"));
                    orders = orders.stream().filter(o -> phrs.contains(o.get_links().get("self").get("href"))).collect(Collectors.toList());
                    break;
                }
                default: {
                    return new ArrayList<>();
                }
            }
            orders.forEach(o -> o.getPlant().setSupplier(supplier));
            return orders;

        } catch (Exception ex){
            return new ArrayList<>();
        }
    }

    public PurchaseOrderDTO sendRemittanceAdvice(Message<PlantHireRequestDTO> message) {
        PlantHireRequestDTO phr = message.getPayload();
        try {
            FullPurchaseOrderDTO po = restTemplate.getForObject(phr.getPurchaseOrder().getHref(), FullPurchaseOrderDTO.class);

            if (po == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No such purchase order");
            }

            return restTemplate.postForEntity(po.get_links().get("remittance").get("href"), null, PurchaseOrderDTO.class).getBody();
        } catch (HttpServerErrorException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot send remittance advice");
        }

    }
}
