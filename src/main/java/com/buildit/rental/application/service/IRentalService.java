package com.buildit.rental.application.service;

import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.rental.application.dto.FullPurchaseOrderDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.buildit.rental.domain.model.POStatus;
import com.buildit.rental.domain.model.PurchaseOrder;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.time.LocalDate;
import java.util.List;

@MessagingGateway(defaultRequestChannel = "req-channel", defaultReplyChannel = "rep-channel")
public interface IRentalService {

    @Gateway(requestChannel = "find-plants")
    List<PlantInventoryEntryDTO> findAvailablePlants(@Payload String name,
                                                     @Header("startDate") LocalDate startDate,
                                                     @Header("endDate") LocalDate endDate);
    @Gateway(requestChannel = "create-po")
    PurchaseOrder createPO(@Payload PlantHireRequestDTO phr);
    @Gateway(requestChannel = "modify-po")
    PurchaseOrder modifyPO(@Payload PlantHireRequestDTO phr);
    @Gateway(requestChannel = "extend-po")
    void extendPO(@Payload String po_href, @Header("date") LocalDate date);
    @Gateway(requestChannel = "cancel-po")
    POStatus cancelPO(@Payload String po_href);

    @Gateway(requestChannel = "send-advice")
    PurchaseOrderDTO sendRemittanceAdvice(@Payload PlantHireRequestDTO phr);

    @Gateway(requestChannel = "find-pos")
    @Payload(value = "new java.util.Date()")
    List<FullPurchaseOrderDTO> findPurchaseOrders();
}
