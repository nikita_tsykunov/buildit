package com.buildit.rental.application.service;

import com.buildit.procurement.rest.ProcurementRestController;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.domain.model.PlantInventoryEntry;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryEntryAssembler
        extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDTO> {

    public PlantInventoryEntryAssembler() {
        super(ProcurementRestController.class, PlantInventoryEntryDTO.class);
    }

    @Override
    public PlantInventoryEntryDTO toResource(PlantInventoryEntry plant) {
        PlantInventoryEntryDTO dto = new PlantInventoryEntryDTO();
        dto.set_id(plant.getPlant_id());
        dto.setName(plant.getPlant_name());
        dto.setHref(plant.getPlant_href());
        dto.setPrice(plant.getPlant_price());
        return dto;
    }
}