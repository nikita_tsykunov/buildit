package com.buildit.rental.application.service;

import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
class CustomTransformer {
    @Autowired
    @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

//    public List<PlantInventoryEntryDTO> fromJson(String json) {
//        try {
//            return mapper.readValue(json, new TypeReference<List<PlantInventoryEntryDTO>>() {});
//        } catch (IOException e) {
//            return null;
//        }
//    }

    //    public CollectionModel<EntityModel<Plant>> fromHALForms(String json) {
//        try {
//            List<Plant> plants = mapper.readValue(json, new TypeReference<List<Plant>>() {});
//            return new CollectionModel<>(plants.stream().map(p -> new EntityModel<>(p, new Link("http://localhost:8090/" + p._id))).collect(Collectors.toList()));
//        } catch (IOException e) {
//            return null;
//        }
//    }
    public List<PlantInventoryEntryDTO> fromHALForms(String json) {
        try {
            return mapper.readValue(json, new TypeReference<List<PlantInventoryEntryDTO>>() {});
        } catch (IOException e) {
            return null;
        }
    }
}
