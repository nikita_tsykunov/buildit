package com.buildit.rental.application.dto;

import com.buildit.common.rest.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseOrderDTO extends ResourceSupport {
    String href;
}