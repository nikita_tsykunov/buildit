package com.buildit.rental.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.procurement.application.dto.InvoiceDTO;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.rental.domain.model.POStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor(force = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FullPurchaseOrderDTO {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    PlantInventoryEntryDTO plant;
    PlantHireRequestDTO plantHireRequest;
    String phrHref;
    @JsonProperty(value = "links")
    private void unpackList(List<Map<String, String>> links) {
        links.forEach(l -> _links.put(l.get("rel"), new HashMap<String, String>() {{
                                                            put("href", l.get("href"));
                                                        }}));
    }

    Map<String, Map<String, String>> _links = new HashMap<>();

    POStatus status;

    String supplierName;
    InvoiceDTO invoice;

    public static FullPurchaseOrderDTO of(BusinessPeriodDTO rentalPeriod, Long plant_id) {
        FullPurchaseOrderDTO fullPurchaseOrderDTO = new FullPurchaseOrderDTO();
        fullPurchaseOrderDTO.rentalPeriod = rentalPeriod;
        fullPurchaseOrderDTO.plant = new PlantInventoryEntryDTO();
        fullPurchaseOrderDTO.plant.set_id(plant_id);
        fullPurchaseOrderDTO.plantHireRequest = new PlantHireRequestDTO();

        return fullPurchaseOrderDTO;
    }
}
