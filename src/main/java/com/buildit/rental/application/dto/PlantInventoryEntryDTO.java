package com.buildit.rental.application.dto;

import com.buildit.common.rest.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlantInventoryEntryDTO extends ResourceSupport {
    Long _id;
    String name;
    String supplier;
    BigDecimal price;
    @JsonProperty(value = "links")
    List<Map<String, String>> _links;
    String href;

    @Override
    public int hashCode() {
        return Objects.hash(_id, name);
    }

    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof PlantInventoryEntryDTO)) return false;

        PlantInventoryEntryDTO plant = (PlantInventoryEntryDTO)other;
        return plant._id.equals(this._id);
    }
}
