package com.buildit.procurement;

import com.buildit.BuilditApplication;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = BuilditApplication.class)
@WebAppConfiguration
public class CreationOfPlantHireRequestSteps {

    private WebDriver driver;
    private final String buildItFrontendURL = "http://ec2-13-53-184-20.eu-north-1.compute.amazonaws.com";
    private final String buildItBackendURL = "http://ec2-13-53-124-92.eu-north-1.compute.amazonaws.com";
    private final String rentItFrontendURL = "http://ec2-18-130-70-94.eu-west-2.compute.amazonaws.com";
    private final String rentItBackendURL = "http://ec2-35-178-104-246.eu-west-2.compute.amazonaws.com";

    static {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
    }

    @Before
    public void setup() throws IOException {
        driver = new ChromeDriver();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        httpClient.execute(new HttpDelete(buildItBackendURL + "/api/procurements/phrs"));
        httpClient.execute(new HttpDelete(rentItBackendURL + "/api/sales/orders"));
        httpClient.close();
    }

    @After
    public void tearOff() throws IOException {
        driver.quit();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        httpClient.execute(new HttpDelete(buildItBackendURL + "/api/procurements/phrs"));
        httpClient.execute(new HttpDelete(rentItBackendURL + "/api/sales/orders"));
        httpClient.close();
    }

    @When("^the user logs in with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void the_user_logs_in(String username, String password) throws InterruptedException {
        WebElement usernameInput = driver.findElement(By.id("username"));
        usernameInput.sendKeys(username);
        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys(password);
        driver.findElement(By.id("login")).click();
        Thread.sleep(100);
    }

    @When("^the user logs out$")
    public void the_user_logs_out() {
        driver.findElement(By.id("logout")).click();
    }

    @Given("^no plant hire requests exist in the system$")
    public void no_plant_hire_requests_exist_in_the_system() {
        driver.get(buildItFrontendURL + "/#/requests");
        List<?> rows = driver.findElements(By.tagName("tr"));
        assertThat(rows.size() - 1).isZero();
    }

    @When("^the site engineer queries the RentIt's plant catalog for an \"([^\"]*)\" available from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void the_site_engineer_queries_the_RentIt_s_plant_catalog_for_an_available_from_to(String plantName, String startDateString, String endDateString) throws Exception {
        driver.get(buildItFrontendURL);
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("name")).isDisplayed());
        WebElement nameInput = driver.findElement(By.id("name"));
        nameInput.sendKeys(plantName);

        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);
        LocalDate now = LocalDate.now();

        long startMonths = ChronoUnit.MONTHS.between(now.withDayOfMonth(1), startDate.withDayOfMonth(1));
        long endMonths = ChronoUnit.MONTHS.between(now.withDayOfMonth(1), endDate.withDayOfMonth(1));

        List<WebElement> dateInputs = driver.findElements(By.cssSelector(".dropdown"));
        WebElement startDateInput = dateInputs.get(0);
        driver.findElement(By.id("startDate")).click();
        for (int i = 0; i < startMonths; i++)
            startDateInput.findElement(By.className("pagination-next")).click();
        WebElement neededStartDate = startDateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(startDate.getDayOfMonth() - 1);
        neededStartDate.click();

        Thread.sleep(300);

        WebElement endDateInput = dateInputs.get(1);
        driver.findElement(By.id("endDate")).click();
        for (int i = 0; i < endMonths; i++)
            endDateInput.findElement(By.className("pagination-next")).click();
        WebElement neededEndDate = endDateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(endDate.getDayOfMonth() - 1);
        neededEndDate.click();

        Thread.sleep(300);
        driver.findElement(By.id("query")).click();
    }

    @Then("^(\\d+) plants are shown$")
    public void plants_are_shown(int numberOfPlants) {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed());
        List<?> rows = driver.findElements(By.tagName("tr"));
        assertThat(rows.size() - 2).isEqualTo(numberOfPlants);
    }

    @When("^the site engineer selects plant number (\\d+)$")
    public void the_site_engineer_selects_plant_number(int plantNumber) {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("select")).isDisplayed());
        driver.findElements(By.id("select")).get(plantNumber - 1).click();
    }

    @When("^the site engineer enters site id (\\d+)$")
    public void the_site_engineer_enters_site_id_and_site_engineer_name(int siteId) {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("siteId")).isDisplayed());
        driver.findElement(By.id("siteId")).sendKeys(String.valueOf(siteId));
    }

    @When("^the site engineer submits plant hire request$")
    public void the_site_engineer_submits_plant_hire_request() throws InterruptedException {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("submitPHR")).isDisplayed());
        driver.findElement(By.id("submitPHR")).click();
        Thread.sleep(100);
    }

    @Then("^a plant hire request is shown with status \"([^\"]*)\" and cost of (\\d+\\.\\d+)$")
    public void a_plant_hire_request_is_shown_with_status_and_cost_of(String status, double cost) {
        driver.get(buildItFrontendURL + "/#/requests");
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed());
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        assertThat(Double.valueOf(rows.get(1).findElements(By.tagName("td")).get(2).getText())).isEqualTo(cost);
        assertThat(rows.get(1).findElements(By.tagName("td")).get(3).getText()).isEqualTo(status);
        assertThat(rows.size()).isEqualTo(2);
    }

    @When("^the works engineer accepts a plant hire request$")
    public void the_works_engineer_accepts_a_plant_hire_request() throws Exception {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("accept")).isDisplayed());
        driver.findElement(By.id("accept")).click();
        Thread.sleep(100);
    }

    @When("^the works engineer rejects a plant hire request$")
    public void the_works_engineer_rejects_a_plant_hire_request() throws Exception {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("reject")).isDisplayed());
        driver.findElement(By.id("reject")).click();
        Thread.sleep(100);
    }

    @Then("^the plant hire request status changes to \"([^\"]*)\"$")
    public void the_plant_hire_request_status_changes_to(String status) {
        driver.get(buildItFrontendURL + "/#/requests");
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed() &&
                        d.findElements(By.tagName("td")).get(3).getText().equals(status));
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        assertThat(rows.get(1).findElements(By.tagName("td")).get(3).getText()).isEqualTo(status);
    }

    @Then("^a RentIt's purchase order is created$")
    public void a_RentIt_s_purchase_order_is_created() {
        driver.get(rentItFrontendURL );
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed());
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        assertThat(rows.size()).isEqualTo(2);
    }

    @When("^the RentIt employee accepts RentIt’s purchase order$")
    public void the_RentIt_employee_accepts_RentIt_s_purchase_order() {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("accept")).isDisplayed());
        driver.findElement(By.id("accept")).click();
    }

    @When("^the RentIt employee rejects RentIt’s purchase order$")
    public void the_RentIt_employee_rejects_RentIt_s_purchase_order() {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("reject")).isDisplayed());
        driver.findElement(By.id("reject")).click();
    }

    @And("^a purchase order is shown in BuildIt with status \"([^\"]*)\"$")
    public void aPurchaseOrderIsShownInBuildItWithStatus(String status) {
        driver.get(buildItFrontendURL + "/#/pos");
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed() &&
                        d.findElements(By.tagName("td")).get(4).getText().equals(status));
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        assertThat(rows.get(1).findElements(By.tagName("td")).get(4).getText()).isEqualTo(status);
    }

    @When("^the site engineer cancels a plant hire request$")
    public void the_site_engineer_cancels_a_plant_hire_request() throws Throwable {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("cancel")).isDisplayed());
        driver.findElement(By.id("cancel")).click();
        Thread.sleep(100);
    }

    @When("^the user modifies a plant hire request$")
    public void the_user_modifies_a_plant_hire_request() throws Throwable {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("modify")).isDisplayed());
        driver.findElement(By.id("modify")).click();
        Thread.sleep(100);
    }

    @When("^changes rental period from \"([^\"]*)\" - \"([^\"]*)\" to \"([^\"]*)\" - \"([^\"]*)\"$")
    public void changes_rental_period_to(String oldStartDateString, String oldEndDateString, String startDateString, String endDateString) throws Throwable {
        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);
        LocalDate oldStartDate = LocalDate.parse(oldStartDateString);
        LocalDate oldEndDate = LocalDate.parse(oldEndDateString);

        long startMonths = ChronoUnit.MONTHS.between(oldStartDate.withDayOfMonth(1), startDate.withDayOfMonth(1));
        long endMonths = ChronoUnit.MONTHS.between(oldEndDate.withDayOfMonth(1), endDate.withDayOfMonth(1));

        List<WebElement> dateInputs = driver.findElements(By.cssSelector(".datepicker.control"));
        WebElement startDateInput = dateInputs.get(0);
        WebElement endDateInput = dateInputs.get(1);

        if (startMonths >= 0) {
            for (int i = 0; i < startMonths; i++)
                startDateInput.findElements(By.className("pagination-next")).get(0).click();
            WebElement neededStartDate = startDateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(startDate.getDayOfMonth() - 1);
            neededStartDate.click();
        }
        else {
            for (long i = startMonths; i < 0; i++)
                startDateInput.findElements(By.className("pagination-previous")).get(0).click();
            WebElement neededStartDate = startDateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(startDate.getDayOfMonth() - 1);
            neededStartDate.click();
        }

        if (endMonths >= 0) {
            for (int i = 0; i < endMonths; i++)
                endDateInput.findElements(By.className("pagination-next")).get(1).click();
            WebElement neededEndDate = endDateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(endDate.getDayOfMonth() - 1);
            neededEndDate.click();
        }
        else {
            for (long i = endMonths; i < 0; i++)
                endDateInput.findElements(By.className("pagination-previous")).get(1).click();
            WebElement neededEndDate = endDateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(endDate.getDayOfMonth() - 1);
            neededEndDate.click();
        }

        driver.findElement(By.id("modify_modal")).click();
        driver.findElement(By.className("modal-close")).click();
        Thread.sleep(100);
    }

    @When("^the site engineer extends a plant hire request from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void the_site_engineer_extends_a_plant_hire_request_to(String oldDateString, String newDateString) {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("extend")).isDisplayed());
        driver.findElement(By.id("extend")).click();
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.cssSelector(".datepicker.control")).isDisplayed());

        LocalDate oldDate = LocalDate.parse(oldDateString);
        LocalDate newDate = LocalDate.parse(newDateString);

        long changedMonths = ChronoUnit.MONTHS.between(oldDate.withDayOfMonth(1), newDate.withDayOfMonth(1));

        WebElement dateInput = driver.findElements(By.cssSelector(".datepicker.control")).get(0);

        if (changedMonths == 0) {
            WebElement neededStartDate = dateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(newDate.getDayOfMonth() - oldDate.getDayOfMonth());
            neededStartDate.click();
        }

        else {
            for (int i = 0; i < changedMonths; i++)
                dateInput.findElements(By.className("pagination-next")).get(0).click();
            WebElement neededStartDate = dateInput.findElements(By.cssSelector(".datepicker-cell.is-selectable")).get(newDate.getDayOfMonth() - 1);
            neededStartDate.click();
        }

        driver.findElement(By.id("extend_modal")).click();
        driver.findElement(By.className("modal-close")).click();
    }

    @Then("^the plant hire request rental period changes to \"([^\"]*)\"$")
    public void the_plant_hire_request_rental_period_changes_to(String targetDates) {
        assertThat(driver.findElements(By.tagName("td")).get(1).getText()).isEqualTo(targetDates);
    }

    @When("^the RentIt employee confirms return of the plant$")
    public void the_RentIt_employee_confirms_return_of_the_plant() {
        driver.get(rentItFrontendURL);
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("dispatch")).isDisplayed());
        driver.findElement(By.id("dispatch")).click();
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("returned")).isDisplayed());
        driver.findElement(By.id("returned")).click();
    }

    @Then("^invoice is shown in RentIt")
    public void invoice_is_shown_in_RentIt() {
        driver.get(rentItFrontendURL + "/#/invoices");
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed());
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        assertThat(rows.size()).isEqualTo(2);
    }

    @And("^invoice has status \"([^\"]*)\"$")
    public void invoice_has_status(String status) {
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed() &&
                        d.findElements(By.tagName("td")).get(3).getText().equals(status));
        assertThat(driver.findElements(By.tagName("td")).get(3).getText()).isEqualTo(status);
    }

    @And("^invoice is shown in BuildIt$")
    public void invoice_is_shown_in_BuildIt() {
        driver.get(buildItFrontendURL + "/#/invoices");
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.tagName("td")).isDisplayed());
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        assertThat(rows.size()).isEqualTo(2);
    }

    @When("^the RentIt system sends reminder$")
    public void the_RentIt_employee_sends_reminder() throws InterruptedException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        httpClient.execute(new HttpPost(rentItBackendURL + "/api/sales/orders/remind"));
        httpClient.close();
        Thread.sleep(300);
        driver.get(rentItFrontendURL + "/#/invoices");
    }

    @When("^the site engineer accepts the invoice$")
    public void the_site_engineer_accepts_the_invoice() throws InterruptedException {
        driver.get(buildItFrontendURL + "/#/invoices");
        (new WebDriverWait(driver, 10)).until(
                (ExpectedCondition<Boolean>) d -> d.findElement(By.id("accept")).isDisplayed());
        driver.findElement(By.id("accept")).click();
        Thread.sleep(100);
    }
}
