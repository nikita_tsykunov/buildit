//package com.buildit.procurement.rest.controller;
//
//import com.buildit.BuilditApplication;
//import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
//import com.buildit.rental.application.service.RentalService;
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import cucumber.api.java.After;
//import cucumber.api.java.en.Given;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.core.io.Resource;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.time.LocalDate;
//import java.util.List;
//
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = {BuilditApplication.class,
//        ProcurementRestControllerTests.RentalServiceMock.class}, properties = {"spring.main.allow-bean-definition-overriding=true"})
//@WebAppConfiguration
//public class ProcurementRestControllerTests {
//    WebDriver driver;
//    @Autowired
//    private WebApplicationContext wac;
//    private MockMvc mockMvc;
//
//    @Autowired
//    @Qualifier("_halObjectMapper")
//    ObjectMapper mapper;
//
//    @Autowired
//    RentalService rentalService;
//
//    @Configuration
//    static class RentalServiceMock {
//        @Bean
//        public RentalService rentalService() {
//            return Mockito.mock(RentalService.class);
//        }
//    }
//    static {
//        System.setProperty("webdriver.chrome.driver", "/chromedriver.exe");
//    }
//
//    @Before
//    public void setup()
//    {
//        driver = new ChromeDriver();
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
//    }
//    @After
//    public void tearoff() {
//        driver.close();
//    }
//
//    @Given("^the following plant catalog$")
//    public void testGetAllPlants() throws Exception {
//        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
//        List<PlantInventoryEntryDTO> list =
//                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() { });
//        LocalDate startDate = LocalDate.now();
//        LocalDate endDate = startDate.plusDays(2);
//        when(rentalService.findAvailablePlants("Truck", startDate, endDate)).thenReturn(list);
//        MvcResult result = mockMvc.perform(
//                get("/api/procurements/plants?name=Truck&startDate={start}&endDate={end}", startDate, endDate))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        // Add test expectations
//    }
//}
//
