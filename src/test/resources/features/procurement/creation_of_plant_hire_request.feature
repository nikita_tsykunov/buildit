Feature: Creation of Plant hire request
  As a BuildIt's employee
  I want to hire all the required machinery
  So that I start with the construction project

  Background: Plant hire requests
    Given no plant hire requests exist in the system
    When the user logs in with username "se" and password "123"

  Scenario: Creation and acceptance of plant hire request
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the user logs out
    And the user logs in with username "we" and password "123"
    And the works engineer accepts a plant hire request
    Then the plant hire request status changes to "ACCEPTED_BY_ENGINEER"
    And a purchase order is shown in BuildIt with status "PENDING"
    And a RentIt's purchase order is created
    When the RentIt employee accepts RentIt’s purchase order
    Then the plant hire request status changes to "ACCEPTED_BY_SUPPLIER"

  Scenario: Creation and modification of plant hire request
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the user modifies a plant hire request
    And changes rental period from "2020-09-22" - "2020-09-24" to "2020-09-22" - "2020-09-25"
    Then a plant hire request is shown with status "PENDING" and cost of 600.00

  Scenario: Rejection of purchase order
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the user logs out
    And the user logs in with username "we" and password "123"
    And the works engineer accepts a plant hire request
    Then the plant hire request status changes to "ACCEPTED_BY_ENGINEER"
    And a RentIt's purchase order is created
    When the RentIt employee rejects RentIt’s purchase order
    Then the plant hire request status changes to "REJECTED_BY_SUPPLIER"

  Scenario: Rejection of plant hire request
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the user logs out
    And the user logs in with username "we" and password "123"
    And the works engineer rejects a plant hire request
    Then the plant hire request status changes to "REJECTED_BY_ENGINEER"

  Scenario: Cancellation of plant hire request before PO acceptance
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the site engineer cancels a plant hire request
    Then the plant hire request status changes to "CANCELED"

  Scenario: Cancellation of plant hire request after PO acceptance
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the user logs out
    And the user logs in with username "we" and password "123"
    And the works engineer accepts a plant hire request
    Then the plant hire request status changes to "ACCEPTED_BY_ENGINEER"
    And a purchase order is shown in BuildIt with status "PENDING"
    And a RentIt's purchase order is created
    When the RentIt employee accepts RentIt’s purchase order
    Then the plant hire request status changes to "ACCEPTED_BY_SUPPLIER"
    When the user logs out
    And the user logs in with username "se" and password "123"
    And the site engineer cancels a plant hire request
    Then the plant hire request status changes to "CANCELED"
    And a purchase order is shown in BuildIt with status "CLOSED"

  Scenario: Extension of plant hire request
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the user logs out
    And the user logs in with username "we" and password "123"
    And the works engineer accepts a plant hire request
    Then the plant hire request status changes to "ACCEPTED_BY_ENGINEER"
    And a purchase order is shown in BuildIt with status "PENDING"
    And a RentIt's purchase order is created
    When the RentIt employee accepts RentIt’s purchase order
    Then the plant hire request status changes to "ACCEPTED_BY_SUPPLIER"
    When the user logs out
    And the user logs in with username "se" and password "123"
    And the site engineer extends a plant hire request from "2020-09-24" to "2020-09-30"
    Then the plant hire request rental period changes to "22.09.2020 - 30.09.2020"
    And a purchase order is shown in BuildIt with status "PENDING"

  Scenario: Getting invoice and its proceeding
    When the site engineer queries the RentIt's plant catalog for an "Excavator" available from "2020-09-22" to "2020-09-24"
    Then 3 plants are shown
    When the site engineer selects plant number 1
    And the site engineer enters site id 1
    And the site engineer submits plant hire request
    Then a plant hire request is shown with status "PENDING" and cost of 450.00
    When the user logs out
    And the user logs in with username "we" and password "123"
    And the works engineer accepts a plant hire request
    Then the plant hire request status changes to "ACCEPTED_BY_ENGINEER"
    And a RentIt's purchase order is created
    When the RentIt employee accepts RentIt’s purchase order
    Then the plant hire request status changes to "ACCEPTED_BY_SUPPLIER"
    When the RentIt employee confirms return of the plant
    Then invoice is shown in RentIt
    And invoice has status "UNPAID"
    And invoice is shown in BuildIt
    And the user logs out
    And the user logs in with username "se" and password "123"
    And invoice has status "UNPAID"

    When the RentIt system sends reminder
    Then invoice has status "LATE_PAYMENT"
    And invoice is shown in BuildIt
    And invoice has status "LATE_PAYMENT"

    When the site engineer accepts the invoice
    Then invoice has status "APPROVED"
    And invoice is shown in RentIt
    And invoice has status "PAID"