import Vue from 'vue'
import Router from 'vue-router'
import OrderCreation from './components/sales/OrderCreation.vue'
import PurchaseOrders from './views/PurchaseOrders.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'order',
      component: OrderCreation
    },
    {
      path: '/requests',
      name: 'requests',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function () { 
        return import(/* webpackChunkName: "about" */ './components/allocations/Requests.vue')
      }
    },
    {
      path: '/invoices',
      name: 'invoices',
      component: function () {
        return import(/* webpackChunkName: "invoices" */ './views/Invoices.vue')
      }
    },
    {
      path: '/pos',
      name: 'purchaseOrders',
      component: PurchaseOrders
    }
  ]
})
