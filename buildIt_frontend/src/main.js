import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import axios from 'axios'

Vue.use(Buefy)

Vue.config.productionTip = false
axios.defaults.baseURL = "http://ec2-13-53-124-92.eu-north-1.compute.amazonaws.com"

new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
