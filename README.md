# BuildIt application #

Contributors: Vladyslav Kupriienko, Stanislav Deviatykh, Nikita Tsykunov, Said Kazimov

RentIt repository: https://bitbucket.org/nikita_tsykunov/rentit/src/master/

Frontends are located in their respective repositories with backends.

Ports:

* BuildIt backend - :8088

* RentIt backend - :8090

* BuildIt frontend - :8095

* RentIt frontend - :8096

To run both frontends, please run "npm install" and "npm run serve" from their respective (frontend) folders.

To run Cucumber acceptance tests, all four processes should be running (both backends and both frontends).

Cucumber tests may behave strangely if some latency occurs during information exchange. Be sure to run tests several times.

API is documented with REST API table in root folder.